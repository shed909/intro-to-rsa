[TOC]

# Introduction
* * *


The RSA (Rivest–Shamir–Adleman) algorithm was named after the 3 scientists/mathematicians who invented it in 1977:

- R - Ron Rivest
- S - Adi Shamir
- A - Leonard Adleman

It is an algorithm for public key encryption and is one of the oldest algorithms of its type.
It aims to create a one-way function that can be used to encrypt and decrypt data between private parties. The data can be transmitted between the two private parties, without the risk of an eavesdropper reading it.

I'll explain the steps we take to generate a public and private key pair using the RSA algorithm, as well as give some practical examples.

We will use small numbers as apposed to extremely large numbers (1024bits+, as seen in the real world), which are $`100`$'s of digits long and would be far to compliated for a practical example.

It is assumed you have some knowledge of algebra and prime number theory.




# Part 1: Generating a Public and Private Key Pair
* * *

## Steps

1. Pick 2 prime numbers, $`p`$ and $`q`$
	- These should both be kept private
	- They will be used to generate our modulus

2. Find the product of $`p`$ and $`q`$, which we will call '$`n`$', eg: $`n = pq`$
	- This will become our modulus
	- We will use it for encrypting and decrypting our data

3. Find $`\phi`$ (Phi) of $`n`$ using Euler's totient function
	- $`\phi(n) = (p-1)(q-1)`$

4. Pick a number, '$`e`$', which is between $`1`$ and $`\phi(n)`$ and is co-prime to $`n`$ and $`\phi(n)`$
	- This will be used for encryption, along with our number $`n`$: $`(n,e)`$
	- This is our "encryption exponent"

5. Find a number $`d`$, such that $`ed ( \bmod \space \phi(n)) = 1`$ using the Extended Euclidean Algorithm
	- This will be used for decryption, along with our number $`n`$: $`(n,d)`$
	- Will always be a multiple of $`e`$
	- This is our "decryption exponent"




## Example A (Alice)
* * *

For this example, let's choose two small prime numbers for our values $`p`$ and $`q`$.
We will use these to generate a public and private key pair.


**Step 1**: Pick 2 prime numbers, $`p`$ and $`q`$

```math
p = 11 \\
q = 43
```

**Step 2**: Find the product of $`p`$ and $`q`$, which we will call '$`n`$', eg: $`n = pq`$

```math
n = pq = 11 \cdot 43 = 473
```

**Step 3**: Find $`\phi`$ (Phi) of $`n`$ using Euler's totient function

```math
\phi(n) = (p-1)(q-1) \\
= (11 - 1) (43 -1) \\
= 10 \cdot 42 = 420
```

**Step 4**: Pick a number, '$`e`$', which is between $`1`$ and $`\phi(n)`$ and is co-prime to $`n`$ and $`\phi(n)`$

```math
\isin \{ 1 < e < \phi(n)\}
```

We can easily pick one by listing the prime factorisation of our value $`\phi(n)`$ (which is $`420`$), then choosing a number between $`1`$ and $`\phi(n)`$ that does not share any factors in common.

```math
420 = 2 \cdot 2 \cdot 3 \cdot 5 \cdot 7 \\
e = 13
```

**Step 5**: Find a number $`d`$, such that $`ed ( \bmod \space \phi(n)) = 1`$ using the Extended Euclidean Algorithm

As an example, we can rely on the Extended Euclidean Algorithm to find the inverse of $`e`$.

|            GCD          |                   13                 |
| ----------------------- | ------------------------------------ |
| $`420-13 \cdot 32=4`$   | $`0-1 \cdot 32=-32`$                 |
| $`13-4 \cdot 3=1`$      | $`1-(-32 \cdot 3)=\underline{97}`$   |
| $`4-1 \cdot 4=0`$




Or, an much easier approach, we can calculate it using Euler's theorem:
```math
e^{-1} \bmod \phi(n) = 13^{-1} \bmod 420 = 97
```

Our inverse of $`e`$ is $`97`$, and we can test that they are congruent:

```math
ed ( \bmod \space \phi(n))  =97*13 \space ( \bmod \space 420) = 1261 \space ( \bmod \space 420) \equiv 1 (\bmod \space 420)
```

Type this in to a calculator and we find that:
```math
1261 \space (\bmod \space 420) = 1
```

Therefore it satisfies our condition of:
```math
ed \space ( \bmod \space \phi(n)) = 1
```

**Alice' keys are:**

Encryption: $`(n,e) = (473,13)`$

Decryption: $`(n,d) = (473,97)`$




## Example B (Bob)
* * *

Since we'll need two parties participating in this exhange, we will go over the same steps again, using different numbers.
These will be Bob's keys.

Feel free to skip over this, and proceed to [Part 2](# Part 2 - Enrypting and Decrypting).

**Step 1**: Pick 2 prime numbers, $`p`$ and $`q`$

```math
p = 7 \\
q = 61
```

**Step 2**: Find the product of $`p`$ and $`q`$, which we will call '$`n`$', eg: $`n = pq`$

```math
n = pq = 7 \cdot 61 = 427
```

**Step 3**: Find $`\phi`$ (Phi) of $`n`$ using Euler's totient function

```math
\phi(n) = (p-1)(q-1) \\
= (7 - 1) (61 -1) \\
= 6 \cdot 60 = 360
```

**Step 4**: Pick a number, '$`e`$', which is between $`1`$ and $`\phi(n)`$ and is co-prime to $`n`$ and $`\phi(n)`$

```math
\isin \{ 1 < e < \phi(n)\}
```

Again, we can pick one by listing the prime factorisation of our value for $`\phi(n)`$ (which is $`960`$), then choosing a number between $`1`$ and $`\phi(n)`$ that does not share any factors in common.

```math
360 = 2 \cdot 2 \cdot 2 \cdot 3 \cdot 3 \cdot 5
```

```math
e = 11
```

**Step 5**: Find a number $`d`$, such that $`ed ( \bmod \space \phi(n)) = 1`$ using the Extended Euclidean Algorithm

| $`2-1 \cdot 2=0`$




Let's calculate it using Euler's theorem:
$`e^{-1} \bmod \phi(n) = 11^{-1} \bmod 360 = 131`$

Our inverse of $`e`$ is $`103`$, and we can test that they are congruent:

```math
ed ( \bmod \space \phi(n))  =11*131 \space ( \bmod \space 360) \\
= 1441 \space ( \bmod \space 360) \equiv 1 (\bmod \space 360)
```

Type this in to a calculator and we find that:

```math
1441 \space (\bmod \space 360) = 1
```

Therefore it satisfies our condition of:

```math
ed \space ( \bmod \space \phi(n)) = 1
```

**Bob's keys are:**

Encryption: $`(n,e) = (427,11)`$

Decryption: $`(n,d) = (427,131)`$



# Part 2: Enrypting and Decrypting
* * *


## Facts

In the previous section, we generated a public/private (encryption/decryption) key pair for both Alice and Bob.
Both parties now have a decryption (private) exponent $`(d)`$, which they will not share publicly, as well as an ecryption (public) exponent $`(e)`$. The encryption exponent can be shared publicly for the other party to use for encrypting messages.


**Example A (Alice)**:



Encryption: $`(n,e) = (473,13)`$

Decryption: $`(n,d) = (473,97)`$



**Example B (Bob)**:



Encryption: $`(n,e) = (427,11)`$

Decryption: $`(n,d) = (427,131)`$




## Encrypting Messages
* * *

To encrypt a message, the function is as follows, where $`c`$ is our resulting cipher text:

```math
F(m,e) = m^e \bmod n = c
```

Bob wants to share a code word with Alice in order to execute a super secret plan to overthrow capitalism.

They have chosen the number $`81`$ in private, as this will indicate to Alice to execute plan C.

We will use Alice' public encryption keys to encrypt the message, which are: $`(n,e) = (473,13)`$

Using the RSA function, Bob can encrypt the message like so:

```math
F(81,13) = 81^{13} \bmod 473 = 53 \\
c = 53
```




## Decrypting Messages
* * *

To decrypt a message, the function is as follows, where $`m`$ is the resulting decrypted message:

```math
F(c,d) = c^d \bmod n = m
```

Alice will decrypt Bob's message using her private keys which she has not published to Bob.

Using the function, he can decrypt the message like so:

```math
F(53,97) = 53^97 \bmod 473 = 81
```

```math
m = 81
```




## Another Example
* * *

Let's try another message using Bob's keys which he has published to Alice.

We'll go with the number $`421`$.

Bob's encryption keys are: $`(n,e) = (427,11)`$

Using the function, Alice can encrypt the message like so:

```math
F(421,11) = 421^11 \bmod 427 = 78
```

```math
c = 78
```

Bob receives this message and decrypts it using his private keys ($`(n,d) = (427,131)`$):

```math
F(c,d) = c^d \bmod n = m
```

```math
F(78,131) = 78^131 \bmod 427 = 421
```




## A Bad Example (Key-size Matters)
* * *


Let's try another message using Bob's keys again.

We'll go with the number $`9083945`$.

Bob's encryption keys are: $`(n,e) = (427,11)`$

Using the function, Alice can encrypt the message like so:

```math
F(9083945,11) = 9083945^11 \bmod 427 = 236
```

```math
c = 236
```

Bob receives this message and decrypts it using his private keys ($`(n,d) = (427,131)`$):

```math
F(c,d) = c^d \bmod n = m
```

```math
F(236,131) = 236^131 \bmod 427 = 374
```

Uh oh! What happened?

The message can not be larger than $`n`$, the modulus, which is referred to as the "key-size".




# Part 3: Breaking Encryption
* * *

Another downside of a small key-size is that we can easily factor this to obtain the private keys.
Suppose we have an eavesdropper who's name is Eve.

To find Bob's private key, all she has to do is calculate:

```math
n = p \cdot q
```

So we already know $n$ and we can sub this in like so:

```math
n = p \cdot q \\
427 = p \cdot q
```

Since $`p`$ and $`q`$ are products of $`n`$, they can't be larger than $`n`$, so let's find the square root of $`n`$:

```math
\sqrt{427} = 20.663978319
```

We know $`p`$ and $`q`$ must be smaller than $`20`$ and one must be smaller than the other, so let's try and solve for $`q`$ by going through the prime numbers between $`1`$ and $`20`$, and see if they are congruent to $`n \bmod c`$, where $`c`$ is our candidate of prime numbers.

```math
427 \bmod 2 = 1 \\
427 \bmod 3 = 1 \\
427 \bmod 5 = 2 \\
427 \bmod \underline{7} = 0
```

We now know our value for $`p`$, and the equation becomes:

```math
427 = 7 \cdot q
```

To find $`q`$, we just have to find a prime number such that $`n - (p \cdot c) = 0`$, where $`c`$ is our candidate.

```math
427 - (7 \cdot 61) = 0 \\
427 = 7 \cdot 61
```

And there you have it. Eve can now generate a public and private key pair from $`p`$ and $`q`$, then decrypt any of Bob's messages.

This was easy enough, but as the numbers get larger, the computational power required to factor them grows exponentially.




# Part 4: Using a Larger Modulus
* * *

Let's try and generate some larger keys with a key-length of 64bits, where $`p`$ and $`q`$ are both 20 digits in size.

### Generate the Keys

We can generate these numbers using openssl, with the folling command:

```bash
openssl prime -generate -bits 64
```


```math
p = \\ 18300021178867331063
```

```math
q = \\ 17133027968424844859
```




**Step 2**: Find the product of $`p`$ and $`q`$, which we will call '$`n`$', eg: $`n = pq`$

```math
n = \\
31353477468030098358\\1327075029166555117
```




**Step 3**: Find $`\phi`$ (Phi) of $`n`$ using Euler's totient function

```math
\phi(n) = \\
(18300021178867331063 - 1) \\ \cdot \\ (17133027968424844859 -1) \\ = \\
3135347746803009835\\45894025881874379196
```




**Step 4**: Pick a number, '$`e`$', which is between $`1`$ and $`\phi(n)`$ and is co-prime to $`n`$ and $`\phi(n)`$

```math
\isin \{ 1 < e < \phi(n)\}
```

Now I'm not going to try and find the prime factorisation of $`\phi(n)`$, as this would take me a very long time.

We can choose a [Fermat number](https://primes.utm.edu/glossary/page.php?sort=FermatNumber), which will always be co-prime.

```math
e = 17
```




**Step 5**: Find a number $`d`$, such that $`ed ( \bmod \space \phi(n)) = 1`$

We're not going to use the Extended Euclidean Algorithm for obvious reasons, so let's calculate it using Euler's theorem or a calculator like [dcode.fr](https://www.dcode.fr/modular-inverse)'s Modular Inverse Calculator:

```math
e^{-1} \bmod \phi(n) = \\ 17^{-1} \bmod 3135347746803009835\\45894025881874379196 = \\ 3688644408003540982\\8928708927279338729
```




Test that this is congruent to $`ed (\bmod \space \phi(n))=1`$.

Type this in to a calculator like [Wolfram Alpha](https://www.wolframalpha.com/input/?i=17+*+36886444080035409828928708927279338729+%28mod+313534774680300983545894025881874379196%29) and we find that:

```math
17 * 36886444080035409828\\928708927279338729 \space \\ (\bmod \space 313534774680300983\\545894025881874379196) \\ \equiv  \\ 1 \space (\bmod \space 313534774680300983\\545894025881874379196)
```

Therefore it satisfies our condition of:
```math
ed \space ( \bmod \space \phi(n)) = 1
```




And we have that:

```math
n = \\ 31353477468030098358\\1327075029166555117 \\
```

```math
e = 17
```
```math
d = \\ 36886444080035409828\\928708927279338729 \\
```




### Encrypt a Message

To encrypt a message, the function is as follows, where $`c`$ is our resulting cipher text:

```math
F(m,e) = m^e \bmod n = c
```




We'll choose something small like $`7`$, since we have ridiculously large numbers to start with and we'll punch this into [Wolfram Alpha](https://www.wolframalpha.com/input/?i=7%5E17+mod+313534774680300983581327075029166555117).

```math
F(m,e) \\
= \\
7^{17} \bmod 3135347746803009835\\81327075029166555117 \\
= \\
232630513987207
```

### Decrypt a Message

To decrypt a message, the function is as follows, where $`m`$ is the resulting decrypted message:

```math
F(c,d) = c^d \bmod n = m
```




Again, let's punch this in to [Wolfram Alpha](https://www.wolframalpha.com/input/?i=232630513987207%5E36886444080035409828928708927279338729+mod+313534774680300983581327075029166555117), and we should find that:

```math
F(232630513987207,\\36886444080035409828928708927279338729) \newline
= \newline
232630513987207^{36886444080035409828928708927279338729} \newline
\bmod \space 313534774680300983581327075029166555117
= \newline
7
```




# References

- DI Management [RSA Algorithm](https://www.di-mgt.com.au/rsa_alg.html), David Ireland
- YouTube.com [The RSA Encryption Algorithm (2 of 2: Generating the Keys)](https://www.youtube.com/watch?v=oOcTVTpUsPQ), Eddie Woo
- RSA Laboratories, [The Mathematics of the RSA Public-Key Cryptosystem](https://www.nku.edu/~christensen/the%20mathematics%20of%20the%20RSA%20cryptosystem.pdf), Burt Kaliski
- sefiks.com [The Math Behind RSA Algorithm](https://sefiks.com/2018/05/21/the-math-behind-rsa-algorithm/), Sefik Ilkin Serengil
- Khan Academy [Euler's totient function](https://www.khanacademy.org/computing/computer-science/cryptography/modern-crypt/v/euler-s-totient-function-phi-function), Brit Cruise
- Drexel University [RSA Calculator](https://www.cs.drexel.edu/~jpopyack/IntroCS/HW/RSAWorksheet.html), JL Popyack
- DOCTRINA [How RSA Works With Examples](https://doctrina.org/How-RSA-Works-With-Examples.html), Barry Steyn
- samsclass.info [Proj RSA2: Cracking a Short RSA Key (15 pts.)](https://samsclass.info/141/proj/pRSA2.htm), Sam Bowne
- Prime Glossary [Fermat number](https://primes.utm.edu/glossary/page.php?sort=FermatNumber), Chris K. Caldwell
- [Wolfram Alpha](https://www.wolframalpha.com/), Wolfram Alpha LLC
